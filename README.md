# CaptureSend2Slack #

## How do I get set up? ##

### 1.Download and import ###

download from [here](https://bitbucket.org/Hiroki_Miyada/capturesend2slack/downloads/CaptureSend2Slack.unitypackage).  

### 2.Edit **Slack Settings** from CaptureSend2Slack.prefab ###

Slack Api Token : [https://api.slack.com/web](https://api.slack.com/web)  
Channel Name : channel name you want to post message  

![capture0.png](https://bitbucket.org/repo/46eAeB/images/2920326481-capture0.png)

![capture1.png](https://bitbucket.org/repo/46eAeB/images/2150097810-capture1.png)

### 3.D&D prefab to scene ###

![capture2.png](https://bitbucket.org/repo/46eAeB/images/4118898241-capture2.png)

## How To Use ##

### 1.Enable capture panel ###

![capture3.png](https://bitbucket.org/repo/46eAeB/images/1551109579-capture3.png)

In UnityEditor, click left and right mouse button simultaneously.  
In Mobile, touch screen with triple fingers.

### 2.Send message and upload capture ###

Enter comment and push "Capture and Send Screenshot" button.